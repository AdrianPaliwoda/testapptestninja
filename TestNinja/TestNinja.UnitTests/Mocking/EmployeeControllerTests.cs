﻿using Moq;
using NUnit.Framework;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class EmployeeControllerTests
    {
        private Mock<IEmployeeRepository> _repo;
        private EmployeeController _employeeController;
        
        [SetUp]
        public void SetUp()
        {
            _repo = new Mock<IEmployeeRepository>();
            _employeeController = new EmployeeController(_repo.Object);
        }

        [Test]
        public void DeleteEmployee_WhenCalled_DeleteEmployeeFromDb()
        {            
            _employeeController.DeleteEmployee(1);
            
            _repo.Verify(r => r.DeleteEmployee(1));
        }
        
        [Test]
        public void DeleteEmployee_WhenCalled_ReturnRedirect()
        {            
           var result = _employeeController.DeleteEmployee(1);
            
            Assert.That( result, Is.TypeOf<RedirectResult>());
        }
    }
}