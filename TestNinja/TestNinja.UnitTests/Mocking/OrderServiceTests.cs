﻿using Moq;
using NUnit.Framework;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class OrderServiceTests
    {
        [Test]
        public void PlaceOrder_WhenCalled_StoreTheOrder()
        {
            var storege = new Mock<IStorage>();
            
            var service = new OrderService(storege.Object);

            var order = new Order();
            service.PlaceOrder(order);
            
            storege.Verify(s => s.Store(order));
        }
    }
}