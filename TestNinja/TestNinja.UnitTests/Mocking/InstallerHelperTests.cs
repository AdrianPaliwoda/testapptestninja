﻿using System;
using System.Net;
using Moq;
using NUnit.Framework;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class InstallerHelperTests
    {
        private Mock<IWebClientFile> _webClient;
        private InstallerHelper _installerHelper;

        [SetUp]
        public void SetUp()
        {
            _webClient = new Mock<IWebClientFile>();
            _installerHelper = new InstallerHelper(_webClient.Object);
        }
        
        [Test]
        public void DownloadInstaller_DownloadFails_ReturnFalse()
        {

            _webClient.Setup(fd => fd.Download(It.IsAny<string>(), null)).Throws<WebException>();
            var result = _installerHelper.DownloadInstaller("customer", "installer");            
            Assert.That(result, Is.False);
            
        }
        
        [Test]
        public void DownloadInstaller_DownloadSuccess_ReturnTrue()
        {
            var result = _installerHelper.DownloadInstaller("customer", "installer");            
            Assert.That(result, Is.True);
            
        }
    }
}