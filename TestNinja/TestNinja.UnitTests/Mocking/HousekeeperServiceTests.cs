using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using TestNinja.Mocking;

namespace TestNinja.UnitTests.Mocking
{
    [TestFixture]
    public class HousekeeperServiceTests
    {
        private HousekeeperService _service;
        private Mock<IStatementGenerator> _statementGenerator;
        private Mock<IEmailSender> _emailSender;
        private Mock<IXtraMessageBox> _messageBox;
        private readonly DateTime _statementDate = new DateTime(2017,1,1);
        private Housekeeper _housekeeper;
        private string _statementFilename;

        [SetUp]
        public void SetUp()
        {
            _housekeeper = new Housekeeper
            {
                Email = "email",
                FullName = "name",
                Oid = 1,
                StatementEmailBody = "statementEmailBody"
            };
            
            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(u => u.Query<Housekeeper>()).Returns(new List<Housekeeper>
            {
                _housekeeper
            }.AsQueryable());

            _statementFilename = "fileName";
            _statementGenerator = new Mock<IStatementGenerator>();
            _statementGenerator
                .Setup(sg => sg.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate))
                .Returns(() => _statementFilename);
            
            _emailSender = new Mock<IEmailSender>();
            _messageBox = new Mock<IXtraMessageBox>();

            _service = new HousekeeperService(
                unitOfWork.Object,
                _statementGenerator.Object,
                _emailSender.Object,
                _messageBox.Object);

        }
        
        [Test]
        public void SendStatementEmail_WhenCalled_GenerateStatements()
        {
            _service.SendStatementEmails(_statementDate);
            _statementGenerator.Verify(sg => 
                sg.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate));
        }       
        
        [Test]
        public void SendStatementEmail_HousekeeperEmailIsNull_ShouldNotGenerateStatement()
        {
            _housekeeper.Email = null;
            
            _service.SendStatementEmails(_statementDate);

            _statementGenerator.Verify(sg => 
                sg.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate), Times.Never);
        }
        
        [Test]
        public void SendStatementEmail_HousekeeperEmailIsWhitespaces_ShouldNotGenerateStatement()
        {
            _housekeeper.Email = " ";
            
            _service.SendStatementEmails(_statementDate);

            _statementGenerator.Verify(sg => 
                sg.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate), Times.Never);
        }       
        
        [Test]
        public void SendStatementEmail_HousekeeperEmailIsEmptyString_ShouldNotGenerateStatement()
        {
            _housekeeper.Email = "";
            
            _service.SendStatementEmails(_statementDate);

            _statementGenerator.Verify(sg => 
                sg.SaveStatement(_housekeeper.Oid, _housekeeper.FullName, _statementDate),
                Times.Never);
        }      
        
        [Test]
        public void SendStatementEmail_WhenCalled_EmailTheStatement()
        {
            _service.SendStatementEmails(_statementDate);
            
            VerifyEmailSend();
        }


        [Test]
        public void SendStatementEmail_StatementFileNameIsNull_ShouldNotEmailTheStatement()
        {
            _statementFilename = null;
            
            _service.SendStatementEmails(_statementDate);
            
            VerifyEmailNotSend();
        }
        
        [Test]
        public void SendStatementEmail_StatementFileNameIsEmptyString_ShouldNotEmailTheStatement()
        {
            _statementFilename = "";
            _service.SendStatementEmails(_statementDate);
            
            VerifyEmailNotSend();
        }
        
        [Test]
        public void SendStatementEmail_StatementFileNameIsWhitespace_ShouldNotEmailTheStatement()
        {
            _statementFilename = " ";

            _service.SendStatementEmails(_statementDate);
            
            VerifyEmailNotSend();
        }    
        
        [Test]
        public void SendStatementEmail_EmailSendingFails_DisplayAMessageBox()
        {
            _emailSender.Setup(es => es.EmailFile(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()
            )).Throws<Exception>();

            _service.SendStatementEmails(_statementDate);
            
            VerifyMessageBoxDisplay();
        }


        private void VerifyEmailSend()
        {
            _emailSender.Verify(es => es.EmailFile(
                _housekeeper.Email,
                _housekeeper.StatementEmailBody,
                _statementFilename,
                It.IsAny<string>()));
        }
        
        private void VerifyEmailNotSend()
        {
            _emailSender.Verify(es => es.EmailFile(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()),
                Times.Never);
        }

        private void VerifyMessageBoxDisplay()
        {
            _messageBox.Verify(b => b.Show(It.IsAny<string>(), It.IsAny<string>(), MessageBoxButtons.OK));
        }
    }
}
