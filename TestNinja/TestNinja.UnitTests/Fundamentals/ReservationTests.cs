﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using TestNinja.Fundamentals;
using Assert = NUnit.Framework.Assert;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class ReservationTests
    {
        private Reservation _reservation;

        [SetUp]
        public void SetUp()
        {
            _reservation = new Reservation();
        }
        
        [Test]
        public void CanBeCallencedBy_AdminCallencing_ReturnsTrue()
        {
            var result = _reservation.CanBeCancelledBy(new User {IsAdmin = true});

            Assert.IsTrue(result);
        }

        [Test]
        public void CanBeCallencedBy_SameUserCallencing_ReturnsTrue()
        {
            User user = new User {IsAdmin = false};
            _reservation.MadeBy = user;
            var result = _reservation.CanBeCancelledBy(user);

            Assert.IsTrue(result);
        }

        [Test]
        public void CanBeCallencedBy_AnotherUserCallencing_ReturnsFase()
        {
            var result = _reservation.CanBeCancelledBy(new User {IsAdmin = false});

            Assert.IsFalse(result);
        }
    }
}
