﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class PhoneNumberTests
    {
        [Test]
        public void CanBePrase()
        {
            string area = "123";
            string major = "456";
            string minor = "7892";

            string number = "1234567892";

            var phoneNumber = PhoneNumber.Parse(number);

            Assert.AreEqual(phoneNumber.Area, area);
            Assert.AreEqual(phoneNumber.Major, major);
            Assert.AreEqual(phoneNumber.Minor, minor);

        }

        [Test]
        public void CanBePrase_MoreThan10Digit_ReturnException()
        {
            string number = "123423567892";

            Assert.That(() => PhoneNumber.Parse(number), Throws.TypeOf<ArgumentException>());

        }

        [Test]
        public void CanBePrase_NullString_ReturnException()
        {
            string number = null;

            Assert.That(() => PhoneNumber.Parse(number), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void CanBePrase_WhiteSpaceInString_ReturnException()
        {
            string number = " \t\t\t ";

            Assert.That(() => PhoneNumber.Parse(number), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void CanBeChangeToString()
        {
            string number = "1234567892";
            string numberToString = "(123)456-7892";

            var phoneNumber = PhoneNumber.Parse(number);

            Assert.AreEqual(phoneNumber.ToString(),numberToString);
      
        }
    }
}
