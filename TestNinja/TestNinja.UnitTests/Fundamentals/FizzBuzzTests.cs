﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]
        [TestCase(15, "FizzBuzz", TestName = "Test values divided by 3 and 5")]
        [TestCase(3, "Fizz", TestName = "Test values divided by 3")]
        [TestCase(5, "Buzz", TestName = "Test values divided by 5")]
        public void GetOutput_Divided_ReturnString(int value, string expectedReturn)
        {
            var result = FizzBuzz.GetOutput(value);

            Assert.AreEqual(expectedReturn, result);
        }

        [Test]
        [TestCase(7, "7")]
        public void GetOutput_NoDividedBy3And5_ReturnNum(int value, string expectedReturn)
        {
            var result = FizzBuzz.GetOutput(value);

            Assert.AreEqual(expectedReturn, result);
        }
    }
}
