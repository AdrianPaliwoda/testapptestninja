﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class HtmlFormatterTests
    {
        [Test]
        [TestCase("bold")]
        public void FormatAsBold_WhenCalled_ShouldEncloseStringWithStongElemntrs(string str)
        {
            var html = new HtmlFormatter();

            var result = html.FormatAsBold(str);

            Assert.That(result, Is.EqualTo("<strong>" + str + "</strong>").IgnoreCase);

            Assert.That(result, Does.StartWith("<strong>").IgnoreCase);
            Assert.That(result, Does.EndWith("</strong>").IgnoreCase);
            Assert.That(result, Does.Contain(str).IgnoreCase);
        }
    }
}
