﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class DemeritPointsCalculatorTests
    {
        private DemeritPointsCalculator _demerit;

        [SetUp]
        public void SetUp()
        {
            _demerit = new DemeritPointsCalculator();;
        }

        [Test]
        [TestCase(30,0)]
        [TestCase(165, 20)]
        public void CalculateDemeritPoints_ReturnValue(int value, int expectedReturnValue)
        {
            var result = _demerit.CalculateDemeritPoints(value);

            Assert.AreEqual(expectedReturnValue, result);
        }
        
        [Test]
        [TestCase(-1)]
        [TestCase(400)]
        public void CalculateDemeritPoints_SpeedOutOfRange_ThrowArgumentOutOfRangeException(int speed)
        {
            Assert.That(() => _demerit.CalculateDemeritPoints(speed), Throws.Exception.TypeOf<ArgumentOutOfRangeException>());
        }


    }
}
