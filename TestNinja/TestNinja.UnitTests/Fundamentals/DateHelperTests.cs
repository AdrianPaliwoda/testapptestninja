﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class DateHelperTests
    {
        [Test]
        public void GetFirstOfMonth_DateTime()
        {
            var data = DateHelper.FirstOfNextMonth(new DateTime(2018, 03, 12));

            Assert.AreEqual(data, new DateTime(2018, 04, 1));
        }
        [Test]
        public void GetFirstOfMonth_DateTimeDec()
        {
            var data = DateHelper.FirstOfNextMonth(new DateTime(2018, 12, 12));

            Assert.AreEqual(data, new DateTime(2019, 1, 1));
        }
    }
}
