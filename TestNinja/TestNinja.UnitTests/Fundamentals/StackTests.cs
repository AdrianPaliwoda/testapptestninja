﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class StackTests
    {
        private Stack<int> _stack;

        [SetUp]
        public void SetUp()
        {
            _stack = new Stack<int>();
        }
        
        [Test]
        [TestCase(12)]
        [TestCase(20)]
        [TestCase(0)]
        [TestCase(-20)]
        public void Length_ValueInt_IsIncreasing(int digit)
        {
            _stack.Push(digit);

            Assert.AreEqual(_stack.Count, 1);
        }

        [Test]
        [TestCase(32)]
        [TestCase(-12)]
        [TestCase(0)]
        public void Length_ValueInt_IsDecreasing(int digit)
        {
            _stack.Push(digit);
            _stack.Push(digit);
            _stack.Pop();

            Assert.AreEqual(_stack.Count, 1);
        }

        [Test]
        [TestCase(12)]
        [TestCase(-12)]
        [TestCase(0)]
        public void Pop_IntValue_ReturnTheSameValue(int digit)
        {
            _stack.Push(digit);
            var result = _stack.Pop();

            Assert.AreEqual(digit, result);

        }
        
        [Test]
        [TestCase(123)]
        [TestCase(0)]
        [TestCase(-123)]
        public void Peek_IntValue_ReturnTheSameValue(int digit)
        {
            _stack.Push(digit);
            var result = _stack.Peek();

            Assert.AreEqual(digit, result);
        }

        [Test]
        public void Push_NullReference_ThrowNullReferenceException()
        {
            object digit = null;

            Assert.ThrowsException<NullReferenceException>(() => _stack.Push((int) digit));
        }

        [Test]
        public void Pop_EmptyStack_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _stack.Pop());
        }
        
        [Test]
        public void Peek_EmptyStack_ThrowInvalidOperationException()
        {
            Assert.ThrowsException<InvalidOperationException>(() => _stack.Peek());
        }
        
        [Test]
        [TestCase(12)]
        public void Peek_IntValue_DoesNotPopValue(int digit)
        {
            _stack.Push(digit);
            _stack.Peek();
            var result = _stack.Peek();

            Assert.AreEqual(digit, result);
        }
    }
}
