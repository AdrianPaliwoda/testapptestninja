﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class ErrorLoggerTests
    {
        private ErrorLogger _errorLogger;
        [SetUp]
        public void SetUp()
        {
            _errorLogger = new ErrorLogger();
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void Log_InvalidError_ThrowArgumentThrowException(string error)
        {
            Assert.That( () =>  _errorLogger.Log(error), Throws.ArgumentNullException );
        }

        [Test]
        [TestCase("message")]
        [TestCase("SecondMessage")]
        public void Log_WhenCalled_SetTheLastError(string message)
        {
            _errorLogger.Log(message);

            Assert.AreEqual(message, _errorLogger.LastError);
        }

        [Test]
        public void Log_ValidError_RaisErrorLoggedEvent()
        {
            var id = Guid.Empty;
            _errorLogger.ErrorLogged += (sender, args) => { id = args; };

            _errorLogger.Log("message");
            
            Assert.That(id, Is.Not.EqualTo(Guid.Empty));
        }
        
    }
}
