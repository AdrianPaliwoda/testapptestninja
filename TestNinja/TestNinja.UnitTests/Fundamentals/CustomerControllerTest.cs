﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class CustomerControllerTest
    {
        private CustomerController _customerController;

        [SetUp]
        public void SetUp()
        {
            _customerController = new CustomerController();
        }
        
        [Test]
        [TestCase(1)]
        public void CanBeFound_NonZeroValue_ReturnActionResultOk(int number)
        {
            var result = _customerController.GetCustomer(number);

            Assert.That(result, Is.TypeOf<Ok>());
        }

        [Test]
        public void GetCustomer_IdIsZero_ReturnNotFound()
        {
            var result = _customerController.GetCustomer(0);
            
            Assert.That(result, Is.TypeOf<NotFound>());
        }
    }
}
