﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Math = TestNinja.Fundamentals.Math;

namespace TestNinja.UnitTests.Fundamentals
{
    [TestFixture]
    public class MathTests
    {
        private Math _math;
    
        [SetUp]
        public void SetUp()
        {
            _math = new Math();
        }
        [Test]
        [Ignore("Because I wanted tot")]
        public void Add_WhenCalled_ReturnTheSumOfArguments()
        {
            var result = _math.Add(1, 2);

            Assert.AreEqual(result,3);
        }

        [Test]
        [TestCase(2, 1, 2)]
        [TestCase(1, 2, 2)]
        [TestCase(1, 1, 1)]
        public void Max_WhenCalled_RetrunGreaterArgument(int a, int b, int expectedResult)
        {
            var result = _math.Max(a, b);

            Assert.AreEqual(result,expectedResult);
        }

        [Test]
        [TestCase(5)]
        public void GetOddNumbers_LimitIsGreaterThanZero_ReturnOddNumbersUpToLimit(int limit)
        {
            var result = _math.GetOddNumbers(limit);

            Assert.That(result, Is.Not.Empty);
            Assert.That(result.Count(), Is.EqualTo(3));

            Assert.That(result, Is.EquivalentTo(new [] {1,3,5}));
            
            int i = 1;
            foreach (var oddNumber in result)
            {
                Assert.AreEqual(i, oddNumber);
                i += 2;
            }
        }
    }
}
