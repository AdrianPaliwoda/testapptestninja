﻿using System.Net;

namespace TestNinja.Mocking
{
    public class InstallerHelper
    {
        private string _setupDestinationFile;
        private IWebClientFile _webClient;

        public InstallerHelper(IWebClientFile webClient = null)
        {
            _webClient = webClient;
        }
        public bool DownloadInstaller(string customerName, string installerName)
        {
            try
            {
                _webClient.Download(
                        string.Format("http://example.com/{0}/{1}",
                        customerName,
                        installerName),
                    _setupDestinationFile);
                
                return true;
            }
            catch (WebException)
            {
                return false; 
            }
        }
    }
}