﻿using System.Net;

namespace TestNinja.Mocking
{
    public interface IWebClientFile
    {
        void Download(string url, string path);
    }

    public class WebClientFile : IWebClientFile
    {
        
        public void Download(string url, string path)
        {
            var client = new WebClient();
            client.DownloadFile(url, path);
        }
    }
}